FROM golang:1.17-alpine as builder

ENV UID=1200
ENV USER=app

WORKDIR /app

COPY go.mod main.go ./ 

RUN adduser --disabled-password \
	--shell "/sbin/nologin" \
	--no-create-home --uid \
	"${UID}" "${USER}" && \
	go mod download && \
	go mod verify

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/main


FROM alpine:3.19.1 

COPY static /go/bin/static
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/main /go/bin/main

USER app:app

EXPOSE 9001

ENTRYPOINT ["/go/bin/main"]
