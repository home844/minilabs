#!/usr/bin/env bash
set -e

#Change password for user WPPG. Then change the password in .env_wordpress
psql -U "$POSTGRES_USER" <<-EOSQL
CREATE DATABASE WPPG;
CREATE USER WPPG WITH PASSWORD 'secret';
GRANT ALL PRIVILEGES ON DATABASE WPPG TO WPPG;
EOSQL

psql -U "$POSTGRES_USER" -d "$WP_DB" <<-EOSQL
GRANT ALL ON SCHEMA public TO WPPG;
EOSQL
