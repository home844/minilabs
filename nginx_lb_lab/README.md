## Load balancing with Nginx and Keepalived

Objective: learn the simplest load balancing scheme. Docker containers imitate hosts.

![alt text](lb_schema.png)
