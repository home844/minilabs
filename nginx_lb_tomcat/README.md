## Build multistage image using Java

Objective: Learn how to build a multistage image using Java and Maven. Docker containers imitate hosts.

![alt text](multi.png)
